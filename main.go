package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
    //	"github.com/gorilla/websocket"
)

func homeRequest(response http.ResponseWriter, request *http.Request){
    fmt.Fprint(response, "Hello")
}

func main () {
    router := mux.NewRouter().StrictSlash(true)
    router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static")))
    log.Print("Starting server at port 8080")
    log.Fatal(http.ListenAndServe(":8080",router))
}

